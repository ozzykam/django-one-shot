from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.


def todo_list_list(request):    
    todos = TodoList.objects.all()
    context = {
        "todolist_object": todos,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoForm()
    
    context = {
        "form":form
    }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm(instance=list)

    context = {
        "list": list,
        "form": form
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        TodoList.delete(delete)
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        # list = TodoList.objects.all()
        # this allows us to populate the list dropdown with list names by first calling the list objects
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItem()
    
    context = {
        "form": form,
        # "list": list,
        # this pushes through to the browser the list object in line 63
    }
    return render(request, "todos/items/create/item_create.html", context)

def todo_item_update(request):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoForm(instance=item)
    
    context = {
        "item": item,
        "form": form,
    }
    return render(request, "todos/update.html")